from gurobipy import *
import re
import math
import numpy as np
from random import choice
import pylab as plt
from scipy.misc import imread


def partie_1(p,d,L,al,coord,Im):
 model=Model()

 #########################lecture des fichiers###################
 n_fichier3=sum(1 for _ in open(coord))
 fichier3=open(coord)
 y=[]
 y1=[]
 y2=[]#coord x
 y3=[]#coord y
 i=0
 while i < n_fichier3:
    c3 = fichier3.readline()

    for e in c3.split(","):
        y.append(e)
    i = i + 1
 i=0

 while i< len(y):
     if math.fmod(i,3):
         y1.append(y[i])
   
     i=i+1
 i=0
 while i< len(y1):
     if math.fmod(i,2):
         y3.append(int(y1[i].rstrip("\n")))
     else:
         y2.append(int(y1[i].rstrip("\n")))
     i=i+1


 n_fichier2= sum(1 for _ in open(d))
 fichier2 = open(d)

 n_fichier = sum(1 for _ in open(p))
 z = []
 H1 = []#la liste des habitants
 m= []#la liste des villes
 I=range(n_fichier)
 d =[]
 i = 0
 fichier = open(p)

 while i < n_fichier:
    c1 = fichier.readline()

    for e in c1.split(","):
        z.append(e)
    i = i + 1

 for a in z:
    if math.fmod(i, 2):
        H1.append(int(a.rstrip("\n")))
    else:
        m.append(a)
    i = i + 1

 
 i=0
 while i< n_fichier2:
    c2=fichier2.readline()
    if i==0 or not math.fmod(i,37):
      i=i+1
    else :
        if math.fmod(i,35):
            c2 = c2.rstrip('\n')
        d.append(c2)
        i=i+1
 #D la matrice de distance 
 D= np.asarray(d,dtype=float).reshape(int(math.sqrt(n_fichier2)),int(math.sqrt(n_fichier2)))
 #avoir les indices des ville composant un secteur
 J=[]
 for i in L:
     J.append(m.index(i))

 H= range(len(H1))

 alpha =((1+al)/len(J))*quicksum(H1[i] for i in range(len(I)))

 ###################"#variable#################################
 Xo =model.addVars(I,J,vtype=GRB.BINARY,name="Xo")

 ####################contraintes###############################

 for j in J:
  model.addConstr((quicksum(H1[i] * Xo[i,j] for i in I )<= alpha),name="c1")#contrainte 1
  model.addConstr(((quicksum(Xo[i,j] for i in I))>=0 ), name="c2")#contrainte 2
  model.addConstr((Xo[j,j] )==1,name="c3")#contrainte3

 for i in I:
  model.addConstr(((quicksum(Xo[i,j] for j in J))==1 ), name="c4")#contrainte4
  
 
 model.addConstr((quicksum(Xo[i,j]for j in J  for i in I)==len(I) ), name="c5")#contrainte 5

 ####################fonction objectif################################
 obj = quicksum ((D[i,j]* Xo[i,j]) for j in J  for i in I)
 model.setObjective(obj,GRB.MINIMIZE)
 #optimisation
 model.optimize()
 sol=[]#variable de solution
 sol1=[]#la liste des secteurs de chaque ville

 if model.status ==GRB.OPTIMAL:

    for v in model.getVars():
        if v.x != 0:
           sol.append(v.varName)
    for i in sol:
        
        sol1.append(int(re.findall('\d+', i)[1]))
   


 sat=[]
 b=0
 for i in sol1:
     
     sat.append(D[b,i])
     b=b+1
 maire=max(sat)
 dis=D[sat.index(max(sat)),sol1[sat.index(max(sat))]]#la distance entre chaque ville et son secteur
 
 satisfaction =sum (sat)/len(sat)#foncion de satisfaction
 

 
 print ("oo")
 print ("oo le moyen des satisfactions est de ",satisfaction)
 print ("oo la distance de la ville du maire le moinssatisfait est de =",dis)
 print ("oo maire le moins satisfait est le maire de:",m[sat.index(max(sat))])        

 ########################### dessiner sur la carte ############################
         
 z = imread(Im)

 couleur=["black","red","green","turquoise","chocolate","yellow","cyan","white"]
 fig, ax = plt.subplots()
 #ax.plot(range(10), range(10), 'o')
 plt.xticks(range(0)[::-1])
 plt.yticks(range(0))
 for i in range(len(sol1)):
     j=J.index(sol1[i])
     
     if sol1[i]==i:
      plt.scatter(y2[i], y3[i], c = couleur[j],marker = '*', edgecolors = couleur[j])
     else:
         if i==sat.index(max(sat)):
             plt.scatter(y2[i], y3[i], c = couleur[j],marker = '.')
         else:
             
            plt.scatter(y2[i], y3[i], c = couleur[j])

 plt.imshow(z)
 plt.show()
 
 return (sat)

 





    
