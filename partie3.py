from gurobipy import *
import re
import math
import numpy as np
from random import choice
import pylab as plt

from scipy.misc import imread
def partie_3(p,d,L,al,coord,Im,f):

 model=Model()
 i=0
 #lecture des fichiers
 n_fichier3=sum(1 for _ in open(coord))
 fichier3=open(coord)
 y=[]
 y1=[]
 y2=[]
 y3=[]
 YY=[]
 while i < n_fichier3:
    c3 = fichier3.readline()

    for e in c3.split(","):
        y.append(e)
    i = i + 1
 i=0

 while i< len(y):
     if math.fmod(i,3):
         y1.append(y[i])
   
     i=i+1
 i=0
 while i< len(y1):
     if math.fmod(i,2):
         y3.append(int(y1[i].rstrip("\n")))
     else:
         y2.append(int(y1[i].rstrip("\n")))
     i=i+1

 n_fichier2= sum(1 for _ in open(d))
 fichier2 = open(d)

 n_fichier = sum(1 for _ in open(p))
 z = []
 H1 = []#la liste des habitants
 m= []#la liste des villes
 I=range(n_fichier)
 d =[]
 i = 0
 fichier = open(p)

 while i < n_fichier:
    c1 = fichier.readline()

    for e in c1.split(","):
        z.append(e)
    i = i + 1

 for a in z:
    if math.fmod(i, 2):
        H1.append(int(a.rstrip("\n")))
    else:
        m.append(a)
    i = i + 1

 
 i=0
 while i< n_fichier2:
    c2=fichier2.readline()
    if i==0 or not math.fmod(i,37):
      i=i+1
    else :
        if math.fmod(i,35):
            c2 = c2.rstrip('\n')
        d.append(c2)
        i=i+1

 D= np.asarray(d,dtype=float).reshape(int(math.sqrt(n_fichier2)),int(math.sqrt(n_fichier2)))
 #la matrice des distances


 H= range(len(H1))


 alpha =((1+al)/L)*quicksum(H1[i] for i in range(len(I)))

 #variable
 y=model.addVars(I,vtype=GRB.INTEGER, lb=0.0, ub=GRB.INFINITY,name="y")#on ajoute la variable y qui a l'indice i
 Xo =model.addVars(I,I,vtype=GRB.BINARY,name="Xo") 
 G=model.addVar()
 #print(H1)
 #contraintes

 for j in I:
  model.addConstr((quicksum(H1[i] * Xo[i,j] for i in I )<= alpha),name="c1")
  model.addConstr(((quicksum(Xo[i,j] for i in I))>=0 ), name="c2")
  model.addConstr(((Xo[j,j])==y[j]),name="c3") # si y[j]=1 alors j est le secteur et x[j,j] sera forcement =1 (meme chose pour 0)
  model.addConstr((quicksum(Xo[i,j] for i in I))>=y[j]) #la somme des ville d'un secteur doit >= a la valeur de y de ce secteur 
  model.addConstr((quicksum(Xo[i,j] for i in I))<=y[j]*len(I))#a somme des ville d'un secteur doit etre <= a n
 for i in I:
  model.addConstr(((quicksum(Xo[i,j] for j in I))==1 ), name="c4")
 for i in I:
  model.addConstr(( G >= quicksum ((D[i,j]* Xo[i,j]) for j in I ) ))
 model.addConstr((quicksum(y[i] for i in I)==L)) #la somme des valeurs des secteur sera egal au k donne
 model.addConstr((quicksum(Xo[i,j]for i in I  for j in I)==len(I) ), name="c5")

 #fonction objectif

 obj1 = quicksum ((D[i,j]* Xo[i,j]) for j in I  for i in I)
 obj=G+ f*obj1
 model.setObjective(obj,GRB.MINIMIZE)

 model.optimize()
 i=0
 so=[]
 soll=[]
 sol=[]
 sol1=[]
 J1=[]
 
 if model.status ==GRB.OPTIMAL:

    for v in model.getVars():
        if v.x != 0:
           soll.append(v.varName)
    


 for s in soll:
     if soll.index(s) <L:
         so.append(s)
     else:
         sol.append(s)


 del sol[-1]
 for i in sol:
        
        sol1.append(int(re.findall('\d+', i)[1]))

 for i in so:
        
        J1.append(int(re.findall('\d+', i)[0]))

         
 sat=[]
 b=0
 for i in sol1:
     
     sat.append(D[b,i])
     b=b+1
 

 maire=max(sat)
 dis=D[sat.index(max(sat)),sol1[sat.index(max(sat))]]
 satisfaction =sum (sat)/len(sat)
 print (sat.index(max(sat)))
 print ("le moyen des satisfactions est de ",satisfaction)
 print ("la distance de la ville du maire le moinssatisfait est de =",dis)
 print ("maire le moins satisfait est le maire de:",m[sat.index(max(sat))])        
                   
 z = imread(Im)

 couleur=["black","red","green","turquoise","chocolate","yellow","cyan","white"]
 fig, ax = plt.subplots()

 plt.xticks(range(0)[::-1])
 plt.yticks(range(0))
 for i in range(len(sol1)):
     j=J1.index(sol1[i])
     if sol1[i]==i:
      plt.scatter(y2[i], y3[i], c = couleur[j],marker = '*', edgecolors = couleur[j])
     else:
         if i==sat.index(max(sat)):
             plt.scatter(y2[i], y3[i], c = couleur[j],marker = '.')
         else:
             
            plt.scatter(y2[i], y3[i], c = couleur[j])

 plt.imshow(z)
 plt.show()

 return (sol1)
     
